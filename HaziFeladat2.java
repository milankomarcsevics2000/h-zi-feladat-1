package hazifeladat2;
import java.util.Random;
import java.util.Scanner;

public class HaziFeladat2 {

    public static void main(String[] args) {
        int [] sajat = new int[5];
        int [] nyero = new int[5];
        int i = 0;
        int f = 0;
        int talalat = 0;
        for(i = 0; i < sajat.length; i++){
            Scanner scn = new Scanner(System.in);
            System.out.print("Adjon meg egy számot: ");
            sajat[i] = scn.nextInt();
            if(sajat[i] < 1 || sajat[i] > 99) {
                System.out.println("A számnak 1 és 99 között kell lennie");
                i--;
            }
            while(f < i){
                if(sajat[f] == sajat[i]){
                    System.out.println("Kétszer ugyanaz a szám nem adható meg");
                    i--;
                }
                f++;
            }
            f = 0;
        }
        Integer min = 1, max = 99;
        i = 0;
        f = 0;
        for(f = 0; f < nyero.length; f++){
            Random rnd = new Random();
            nyero[f] = rnd.nextInt((max - min) + min) + min;
            while(i < f){
                if(nyero[f] == nyero[i]){
                    nyero[f] = rnd.nextInt((max - min) + min) + min;
                }
                i++;
            }
            i = 0;
        }
        for(f = 0; f < nyero.length; f++){
            for(i = 0; i < sajat.length; i++){
                if(nyero[f] == sajat[i]){
                    System.out.println("A(z) "+sajat[i]+" számot eltalálta!");
                    talalat++;
                }
            }
        }
        System.out.println("Önnek "+talalat+" találata volt.");
    }
    
}
